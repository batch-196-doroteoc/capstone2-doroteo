const Product =require("../models/Product");

module.exports.getAllActiveProducts = (req,res)=>{

	Product.find({isActive:true})
	.then(result=>res.send(result))
	.catch(error=>res.send(error))

};

module.exports.createProduct = (req,res)=>{
	
	let newProduct = new Product({
		name: req.body.name,
		description: req.body.description,
		price: req.body.price
	})
	
	newProduct.save()
	.then(result=>res.send(result))
	.catch(error=>res.send(error))
};

module.exports.getSingleProduct = (req,res)=>{
	console.log(req.params)
	console.log(req.params.productId)
	Product.findById(req.params.productId)
	.then(result => res.send(result))
	.catch(error => res.send(error))
};

module.exports.updateProductInfo = (req,res) =>{
	console.log(req.params.productId);
	console.log(req.body);
	let updatedinfo = {
		name:req.body.name,
		description:req.body.description,
		price:req.body.price
	}
	Product.findByIdAndUpdate(req.params.productId,updatedinfo,{new:true})
	.then(result=> res.send(result))
	.catch(error=> res.send(error))
};

module.exports.archiveProduct = (req,res) => {

	console.log(req.params.productId);
	let update = {
		isActive: false
	}

	Product.findByIdAndUpdate(req.params.productId,update,{new:true})
	.then(result=>res.send(result))
	.catch(error=> res.send(error))
};


////////////////////additional controllers
module.exports.unarchiveProduct = (req,res) => {

	console.log(req.params.productId);
	let update = {
		isActive: true
	}

	Product.findByIdAndUpdate(req.params.productId,update,{new:true})
	.then(result=>res.send(result))
	.catch(error=> res.send(error))
};

module.exports.getAllInactiveProducts = (req,res)=>{

	Product.find({isActive:false})
	.then(result=>res.send(result))
	.catch(error=>res.send(error))

};
