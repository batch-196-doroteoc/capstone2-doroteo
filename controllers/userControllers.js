const User = require("../models/User");
const Product = require("../models/Product");
const Order = require("../models/Order");
const bcrypt = require("bcrypt");
const auth = require("../auth");

module.exports.registerUser = (req,res)=>{

	const hashedPw = bcrypt.hashSync(req.body.password,10);
	// console.log(hashedPw);

	let newUser = new User({

		firstName: req.body.firstName,
		lastName: req.body.lastName,
		email: req.body.email,
		password: hashedPw,
		mobileNo: req.body.mobileNo

	})

	newUser.save()
	.then(result=>res.send(result))
	.catch(error=>res.send(error))

};

module.exports.loginUser = (req,res)=>{
	console.log(req.body);

	User.findOne({email:req.body.email})
	.then(foundUser=>{
		
		if(foundUser === null){
			return res.send({message: "No User Found."})
		}else{
			
			const isPasswordCorrect = bcrypt.compareSync(req.body.password,foundUser.password);
			
			if(isPasswordCorrect){
				return res.send({accessToken: auth.createAccessToken(foundUser)});
			}else{
				return res.send({message:"Incorrect Password"});
			}
		}
	})
};

module.exports.makeAdmin = (req,res) => {

	console.log(req.params.userId);
	let update = {
		isAdmin: true
	}

	User.findByIdAndUpdate(req.params.userId,update,{new:true})
	.then(result=>res.send(result))
	.catch(error=> res.send(error))
};

module.exports.getAllOrders = (req,res)=>{

	Order.find({})
	.then(result=>res.send(result))
	.catch(error=>res.send(error))
};

module.exports.getUserDetails = (req,res)=>{
	console.log(req.user)
	User.findById(req.user.id)
	.then(result=>res.send(result))
	.catch(error=>res.send(error))
};

module.exports.getUserOrders= (req,res)=>{
	if(req.user.isAdmin){
            res.send({message:"Admin is not allowed to retrieve a registered user's order!"})
        }
	Order.find({id:req.user.id})
	.then(result=>res.send(result))
	.catch(error=>res.send(error))
};

/////////additional controllers

module.exports.demoteAdmin = (req,res) => {

	console.log(req.params.userId);
	let update = {
		isAdmin: false
	}

	User.findByIdAndUpdate(req.params.userId,update,{new:true})
	.then(result=>res.send(result))
	.catch(error=> res.send(error))
};