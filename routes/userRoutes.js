const express = require("express");
const router = express.Router();
const bcrypt = require("bcrypt");
const userControllers = require("../controllers/userControllers");
const auth = require("../auth");
const { verify,verifyAdmin } = auth;
console.log(userControllers);

//User Registration
router.post("/",userControllers.registerUser);

//User Authentication
router.post('/login',userControllers.loginUser);

//Set User as Admin (ADMIN ONLY)
router.put('/updateAdmin/:userId',verify,verifyAdmin,userControllers.makeAdmin);

//Retrieve All Orders (ADMIN ONLY)
router.get('/getAllOrders',verify,verifyAdmin,userControllers.getAllOrders);

// router.post('/createOrder',verify,userControllers.order);

//Get User Details, (No request body, needs token)
router.get('/details',verify,userControllers.getUserDetails);

//Retrieve Authenticated Users Orders
router.get('/myOrders',verify,userControllers.getUserOrders);

/////additional routes

//Set Admin - false
router.put('/demoteAdmin/:userId',verify,verifyAdmin,userControllers.demoteAdmin);
module.exports = router;