const express = require("express");
const router = express.Router();
const orderControllers = require("../controllers/orderControllers");
const auth = require("../auth");
const { verify,verifyAdmin } = auth;
console.log(orderControllers);

//Logged in Regular User Checkout
router.post('/',verify,orderControllers.createOrder);

//Retrieve All Orders (ADMIN ONLY)
router.get('/',verify,verifyAdmin,orderControllers.getAllOrders);

//Retrieve Authenticated Users Orders
router.get('/getUserOrders',verify,orderControllers.getUserOrders);

////additional routes
//Display User Products Per Order
router.get('/myOrders/:orderId',verify,orderControllers.getUserProductPerOrder);
module.exports = router;
